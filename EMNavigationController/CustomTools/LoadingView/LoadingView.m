//
//  LoadView.m
//  LLYG
//
//  Created by EasonWang on 13-9-24.
//  Copyright (c) 2013年 ShiTengTechnology. All rights reserved.
//


// 中心点坐标
#define CENTER_X(_objc_) (YES?_objc_.frame.size.width/2 : 0)
#define CENTER_Y(_objc_) (YES?_objc_.frame.size.height/2 : 0)

#define DEFAULT_WIDTH [[UIScreen mainScreen]bounds].size.width
#define DEFAULT_HEIGHT [[UIScreen mainScreen]bounds].size.height


#import "LoadingView.h"
#import <QuartzCore/QuartzCore.h>
#import "AutoDismissAlertView.h"

@interface LoadingView ()
{
    UIWindow *_overlayWindow;
}
@end


@implementation LoadingView

- (void)createOverlayWindow
{
	_overlayWindow = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
	_overlayWindow.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
	_overlayWindow.backgroundColor = [UIColor clearColor];
	[_overlayWindow makeKeyAndVisible];
	[_overlayWindow addSubview:self];
}


//@synthesize mText;
UIViewController *mController ;

-(id)initWithDefault
{
    self = [super initWithFrame:CGRectMake(0, 0, 200, 200)];
    if (self) {
        self.center = CGPointMake(DEFAULT_WIDTH/2,DEFAULT_HEIGHT/2);
        [self willShow];
    }
    return self;
}

- (id)init
{
    self = [super initWithFrame:CGRectMake(0, 0, 200, 200)];
    if (self) {
        self.center = CGPointMake(DEFAULT_WIDTH/2,DEFAULT_HEIGHT/2);
        [self willShow];
    }
    return self;
}

+ (id)loadingView
{
    LoadingView *load = [[LoadingView alloc] initWithDefault];
    return load;
}

-(id)initWithDefault:(UIViewController*)controller
{
    self = [super initWithFrame:CGRectMake(0, 0, 200, 200)];
    if (self) {
        mController = controller;
        self.center = CGPointMake(DEFAULT_WIDTH/2,DEFAULT_HEIGHT/2);
        [self willShow];
    }
    return self;
}

- (id)initWithView:(UIView*) view {
    id me = [self initWithFrame:view.bounds];
    return me;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.center = CGPointMake(frame.size.width/2, frame.size.height/2);
        [self willShow];
    }
    return self;
}

- (void) willShow { // must call this method after init with xib
    self.clipsToBounds = YES;
    self.backgroundColor = [UIColor colorWithWhite: 0.0 alpha: 0.8f];
    self.layer.cornerRadius = 10.0f;
    mJuhua = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    mJuhua.hidesWhenStopped = YES;
    mJuhua.center = CGPointMake(self.bounds.size.width/2, self.bounds.size.height/2+20);
    [self addSubview:mJuhua];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 30)];
    label.font = [UIFont systemFontOfSize:15];
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor whiteColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.center = CGPointMake(self.bounds.size.width/2, self.bounds.size.height/2-20);
    label.text = @"加载中...";
    [self addSubview:label];
    self.hidden = YES;
}

- (void) willShowWithMessage:(NSString*)text { // must call this method after init with xib
    self.clipsToBounds = YES;
    self.backgroundColor = [UIColor colorWithWhite: 0.0 alpha: 0.8f];
    self.layer.cornerRadius = 10.0f;
    mJuhua = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    mJuhua.hidesWhenStopped = YES;
    mJuhua.center = CGPointMake(self.bounds.size.width/2, self.bounds.size.height/2+20);
    [self addSubview:mJuhua];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 30)];
    label.font = [UIFont systemFontOfSize:15];
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor whiteColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.center = CGPointMake(self.bounds.size.width/2, self.bounds.size.height/2-20);
    label.text = text;
    [self addSubview:label];
    self.hidden = YES;
}

- (void) startAnimating {
    [self createOverlayWindow];
    [mJuhua startAnimating];
    self.hidden = NO;
    loadTimer = [NSTimer scheduledTimerWithTimeInterval:16.5f target:self selector:@selector(showErrorMessage) userInfo:nil repeats:NO];
}

- (void) stopAnimating {
    _overlayWindow = nil;
    [mJuhua stopAnimating];
    self.hidden = YES;
    [loadTimer invalidate];
}

-(void)showErrorMessage
{
    [self stopAnimating];
    AutoDismissAlertView *alert = [[AutoDismissAlertView alloc]initWithTitle:@"" message:@"数据加载失败！" delegate:self dismissAfterSeconds:2.0f animated:YES];
    [alert show];
    [self.lDelegate loadViewFailure];
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

@end
